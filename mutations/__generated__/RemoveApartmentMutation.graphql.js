/**
 * @flow
 * @relayHash c98f4520d75908fdf177c663670b6e03
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type RemoveApartmentMutationVariables = {|
  input: {
    id: string,
    clientMutationId?: ?string,
  },
|};
export type RemoveApartmentMutationResponse = {|
  +removeApartment: ?{|
    +removedId: ?string,
  |},
|};
*/


/*
mutation RemoveApartmentMutation(
  $input: RemoveApartmentInput!
) {
  removeApartment(input: $input) {
    removedId
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "RemoveApartmentInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "removeApartment",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "RemoveApartmentInput!"
      }
    ],
    "concreteType": "RemoveApartmentPayload",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "removedId",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "RemoveApartmentMutation",
  "id": null,
  "text": "mutation RemoveApartmentMutation(\n  $input: RemoveApartmentInput!\n) {\n  removeApartment(input: $input) {\n    removedId\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "RemoveApartmentMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "RemoveApartmentMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
(node/*: any*/).hash = 'bb66fa856fdeb807add9f2866b6506c9';
module.exports = node;
