/**
 * @flow
 * @relayHash b5a59cfc120b463586eb239d36a231c8
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type AddApartmentMutationVariables = {|
  input: {
    title: string,
    slug?: ?string,
    area?: ?number,
    rooms?: ?number,
    type?: ?string,
    price?: ?number,
    fullAdress?: ?string,
    clientMutationId?: ?string,
  },
|};
export type AddApartmentMutationResponse = {|
  +addApartment: ?{|
    +apartmentEdge: ?{|
      +cursor: string,
      +node: ?{|
        +id: string,
        +title: ?string,
        +slug: ?string,
      |},
    |},
  |},
|};
*/


/*
mutation AddApartmentMutation(
  $input: AddApartmentInput!
) {
  addApartment(input: $input) {
    apartmentEdge {
      cursor
      node {
        id
        title
        slug
      }
    }
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "input",
    "type": "AddApartmentInput!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "addApartment",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input",
        "type": "AddApartmentInput!"
      }
    ],
    "concreteType": "AddApartmentPayload",
    "plural": false,
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "name": "apartmentEdge",
        "storageKey": null,
        "args": null,
        "concreteType": "ApartmentEdge",
        "plural": false,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "name": "cursor",
            "args": null,
            "storageKey": null
          },
          {
            "kind": "LinkedField",
            "alias": null,
            "name": "node",
            "storageKey": null,
            "args": null,
            "concreteType": "Apartment",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "id",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "title",
                "args": null,
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "name": "slug",
                "args": null,
                "storageKey": null
              }
            ]
          }
        ]
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "mutation",
  "name": "AddApartmentMutation",
  "id": null,
  "text": "mutation AddApartmentMutation(\n  $input: AddApartmentInput!\n) {\n  addApartment(input: $input) {\n    apartmentEdge {\n      cursor\n      node {\n        id\n        title\n        slug\n      }\n    }\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "AddApartmentMutation",
    "type": "Mutation",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "AddApartmentMutation",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
(node/*: any*/).hash = '38d13aa759461c60a3a136464fbc43a4';
module.exports = node;
