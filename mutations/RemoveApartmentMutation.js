import {
    commitMutation,
    graphql,
} from 'react-relay';
import {ConnectionHandler} from "relay-runtime";
import environment from '../lib/env';


const mutation = graphql`
    mutation RemoveApartmentMutation($input: RemoveApartmentInput!) {
        removeApartment(input: $input) {
            removedId,
        }
    }
`;

function commit(
    id,
) {
    return commitMutation(
        environment,
        {
            mutation,
            variables: {
                input: { id },
            },
            onCompleted(res, err) {
                if(err) {
                    console.log('ERROR', err)
                }
                console.log('ERROR', res)
            },
            onError(err) {
                console.log(err)
            },
            updater(store, data) {
                const root = store.getRoot();
                const conn = ConnectionHandler.getConnection(
                  root,
                  'admin_apartments'
                );
                ConnectionHandler.deleteNode(conn, id);

            }
        }
    );
}

export default { commit };
