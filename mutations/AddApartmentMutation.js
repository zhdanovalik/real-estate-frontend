import {
    commitMutation,
    graphql,
} from 'react-relay';
import {
    ConnectionHandler
} from 'relay-runtime';
import environment from '../lib/env';

const mutation = graphql `
    mutation AddApartmentMutation($input: AddApartmentInput!) {
        addApartment(input:$input) {
            apartmentEdge {
                cursor
                node {
                  id
                  title
                  slug
                }
            }
        }
    }
  `;

function commit(
    apartment
) {
    return new Promise((resolve, reject) => commitMutation(
        environment,
        {
            mutation,
            variables: {
                input: apartment,
            },
            onCompleted: (response, errors) => {
                if(errors) {
                    reject(errors);
                }
                resolve();
            },
            onError: err => reject(err),
            updater: (store, data) => {
                const root = store.getRoot();
                const conn = ConnectionHandler.getConnection(
                    root,
                    'admin_apartments'
                );

                const payload = store.getRootField('addApartment');
                const newEdge = payload && payload.getLinkedRecord('apartmentEdge');
                (conn && newEdge) && ConnectionHandler.insertEdgeAfter(conn, newEdge);
            }
        }
    ));
}

export default {
    commit
};
