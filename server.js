const express = require('express')
const bodyParser = require('body-parser');
const next = require('next');
const proxy = require('http-proxy-middleware');

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
    .then(() => {
        const server = express()

        server.use(bodyParser.json());
        server.use(bodyParser.urlencoded({ extended: false }));

        server.use('/api', proxy({ target: 'http://localhost:7799', changeOrigin: true }))
        server.use('/graphql', proxy({ target: 'http://localhost:7799', changeOrigin: true }))

        server.get('/appartements/:slug', (req, res) => {
            const queryParams = { slug: req.params.slug }
            return app.render(req, res, '/appartements', queryParams)
        })

        server.get('/admin/apartment/:slug', (req, res) => {
            return app.render(req, res, '/admin/apartment', { slug: req.params.slug })
        })

        server.get('*', (req, res) => {
            return handle(req, res)
        })

        server.listen(8877, (err) => {
            if (err) throw err
            console.log('> Ready on http://localhost:8877')
        })
    })
    .catch((ex) => {
        console.error(ex.stack)
        process.exit(1)
    })