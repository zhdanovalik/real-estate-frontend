import React from 'react'
import styled from 'styled-components'
import Layout from '../compnents/MyLayout.js'
import Link from 'next/link'
import Hero from '../compnents/Hero'
import Section from '../compnents/Section'
import AboutUs from '../compnents/AboutUs'
import Apartments from '../compnents/Apartments'
import Contact from '../compnents/Contact'

const Index = (props) => (
  <Layout>
    <Hero />
    <AboutUs id="about-us" />
    <Apartments id="apartments" />
    <Contact />
  </Layout>
)

// Index.getInitialProps = async function() {
//   const res = await fetch('https://api.tvmaze.com/search/shows?q=batman')
//   const data = await res.json()

//   console.log(`Show data fetched. Count: ${data.length}`)

//   return {
//     shows: data
//   }
// }

export default Index