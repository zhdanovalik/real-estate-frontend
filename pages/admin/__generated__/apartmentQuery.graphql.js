/**
 * @flow
 * @relayHash 6db599cc810036b3be3cdb1a10b0cfa4
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type apartmentQueryVariables = {|
  slug: string,
|};
export type apartmentQueryResponse = {|
  +apartment: ?{|
    +id: string,
    +title: ?string,
    +slug: ?string,
    +area: ?number,
    +rooms: ?number,
    +type: ?string,
    +fullAdress: ?string,
  |},
|};
*/


/*
query apartmentQuery(
  $slug: String!
) {
  apartment(slug: $slug) {
    id
    title
    slug
    area
    rooms
    type
    fullAdress
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "kind": "LocalArgument",
    "name": "slug",
    "type": "String!",
    "defaultValue": null
  }
],
v1 = [
  {
    "kind": "LinkedField",
    "alias": null,
    "name": "apartment",
    "storageKey": null,
    "args": [
      {
        "kind": "Variable",
        "name": "slug",
        "variableName": "slug",
        "type": "String"
      }
    ],
    "concreteType": "Apartment",
    "plural": false,
    "selections": [
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "id",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "title",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "slug",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "area",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "rooms",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "type",
        "args": null,
        "storageKey": null
      },
      {
        "kind": "ScalarField",
        "alias": null,
        "name": "fullAdress",
        "args": null,
        "storageKey": null
      }
    ]
  }
];
return {
  "kind": "Request",
  "operationKind": "query",
  "name": "apartmentQuery",
  "id": null,
  "text": "query apartmentQuery(\n  $slug: String!\n) {\n  apartment(slug: $slug) {\n    id\n    title\n    slug\n    area\n    rooms\n    type\n    fullAdress\n  }\n}\n",
  "metadata": {},
  "fragment": {
    "kind": "Fragment",
    "name": "apartmentQuery",
    "type": "Root",
    "metadata": null,
    "argumentDefinitions": v0,
    "selections": v1
  },
  "operation": {
    "kind": "Operation",
    "name": "apartmentQuery",
    "argumentDefinitions": v0,
    "selections": v1
  }
};
})();
(node/*: any*/).hash = '76d617f6f486770e0dc14ab791cbb920';
module.exports = node;
