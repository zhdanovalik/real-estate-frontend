import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay'
import Link from 'next/link'
import Container from '../../compnents/Container'
import RemoveApartmentMutation from '../../mutations/RemoveApartmentMutation';
import environment from '../../lib/env';

const Admin = ({apartments, environment}) => {
    console.log(apartments)
    return (
        <Container>
            <h1>Admin panel</h1>
            <ul>
                {apartments && apartments.edges && apartments.edges.map(({node}) => (
                    <li key={
                        node.id}>
                        <Link href={{ pathname: '/admin/apartment', query: { slug: node.slug } }}><a>{node.title}</a></Link>
                        <button onClick={() => {
                            RemoveApartmentMutation.commit(
                                node.id
                            )}}>x</button>
                    </li>
                ))}
            </ul>

        </Container>
    )
};

const BookingsListRenderer = (props) => {
    return (
        <QueryRenderer
            environment={environment}
            query={graphql`
            query admin_indexQuery {
                apartments(first: 214748364) @connection(key: "admin_apartments") {
                    edges {
                        node {
                            id,
                            title,
                            slug
                        }
                    }
                }
            }
          `}
            render={({ error, props }) => {
                if (props) {
                    return (
                        <Admin { ...props}/>
                    );
                } else {
                    return <div>loading</div>
                }
            }}
        />
    );
};

export default BookingsListRenderer;
