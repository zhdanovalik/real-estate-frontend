import React, { Component } from 'react';
import { QueryRenderer, graphql } from 'react-relay';
import environment from '../../lib/env';
import Link from 'next/link'
import Container from '../../compnents/Container'

class Apartment extends Component {
    constructor(props) {
        super(props);
        const { title } = this.props.apartment;
        this.state = {
            title,
        };
    }
    static getInitialProps ({ query: { slug } }) {
        return { slug }
    }

    render () {
        const { slug } = this.props.apartment;
        const { title } = this.state;

        return <Container>
            <h1>
                My blog post - {slug} -
                <Link href={{ pathname: '/admin/apartment/edit', query: { slug: slug } }}><a>Edit</a></Link>
            </h1>
            <input type="text" value={title}/>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. aaa
            </p>

            <Link href={{ pathname: '/admin' }}><a>Home</a></Link>
        </Container>
    }
}

// export default withData(Apartment, {
//     query: graphql`
//         query apartmentQuery {
//             apartment {
//                 id,
//                 title,
//                 slug
//             }
//         }
//     `
// })

export default props => {
    return (
        <QueryRenderer
            environment={environment}
            query={graphql`
                query apartmentQuery($slug: String!) {
                    apartment(slug: $slug) {
                        id,
                        title,
                        slug
                        area
                        rooms
                        type
                        fullAdress
                    }
                }
            `}
            variables={{ slug: props.url.query.slug }}
            render={({ error, props }) => {
                if (props) {
                    return (
                        <Apartment { ...props}/>
                    );
                } else {
                    return <div>loading</div>
                }
            }}
        />
    )
}
