import React, { Component } from 'react'
import Router from 'next/router'
import withData from '../../lib/withData'
import AddApartmentMutation from '../../mutations/AddApartmentMutation'

class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            area: 0,
            rooms: 0,
            type: '',
            price: 0,
            fullAdress: ''
        };
    }

    handleInputChange = ({ target }) => {
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      };

    handleSubmit = (evt) => {
        evt.preventDefault();
        AddApartmentMutation
            .commit(
                this.state
            )
            .then(() => Router.push('/admin'))
    };

    render() {
        const { 
            title,
            area, 
            rooms, 
            type, 
            price, 
            fullAdress 
        } = this.state;
        
        return (
            <div>
                <h1>New Apartment</h1>
                <form onSubmit={this.handleSubmit}>
                    <div>
                        Titile: 
                        <input 
                            type="text" 
                            name="title" 
                            value={title}
                            onChange={this.handleInputChange} />
                    </div>
                    <div>
                        Area: 
                        <input 
                            type="number" 
                            name="area" 
                            value={area} 
                            onChange={this.handleInputChange} />
                    </div>
                    <div>
                        Rooms: 
                        <input 
                            type="number" 
                            name="rooms" 
                            value={rooms} 
                            onChange={this.handleInputChange} />
                    </div>
                    <div>
                        Type: 
                        <input 
                            type="text" 
                            name="type" 
                            value={type} 
                            onChange={this.handleInputChange} />
                    </div>
                    <div>
                        Price: 
                        <input 
                            type="number" 
                            name="price" 
                            value={price} 
                            onChange={this.handleInputChange} />
                    </div>
                    <div>
                        Adress: 
                        <input 
                            type="text" 
                            name="fullAdress" 
                            value={fullAdress} 
                            onChange={this.handleInputChange} />
                    </div>
                    <button>Submit</button>
                </form>
            </div>
        )
    }
}

export default New;
